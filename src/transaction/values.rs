// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: AGPL-3.0-or-later
use std::fmt;

use serde::Deserialize;

/// The different types of values we accept as "expected"
/// Numeric, String, Bool.
/// Separate from `Values` as this one is "untagged".
/// See <https://serde.rs/enum-representations.html>
#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub(crate) enum ExpectedValues {
    Numeric(f64),
    String(String),
    Bool(bool),
}

/// The different types of values we accept  `SenML` values (v, vs, vb) for the "v" field:
/// Separate from `ExpectedValues` as this one is "tagged", thus parsing Each field "v /vs/ vb" and
/// internalizing them..
/// See <https://serde.rs/enum-representations.html>
#[derive(Debug, Deserialize)]
pub(crate) enum Values {
    #[serde(rename = "v")]
    Numeric(f64),
    #[serde(rename = "vs")]
    String(String),
    #[serde(rename = "vb")]
    Bool(bool),
}

impl std::convert::From<ExpectedValues> for Values {
    fn from(val: ExpectedValues) -> Self {
        match val {
            ExpectedValues::Numeric(num) => Self::Numeric(num),
            ExpectedValues::String(s) => Self::String(s),
            ExpectedValues::Bool(b) => Self::Bool(b),
        }
    }
}

impl std::convert::From<Values> for ExpectedValues {
    fn from(val: Values) -> Self {
        match val {
            Values::Numeric(num) => Self::Numeric(num),
            Values::String(s) => Self::String(s),
            Values::Bool(b) => Self::Bool(b),
        }
    }
}

impl fmt::Display for Values {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Numeric(x) => write!(f, "{x}"),
            Self::Bool(x) => write!(f, "{x}"),
            Self::String(x) => write!(f, "{x}"),
        }
    }
}

impl fmt::Display for ExpectedValues {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Numeric(x) => write!(f, "{x}"),
            Self::Bool(x) => write!(f, "{x}"),
            Self::String(x) => write!(f, "{x}"),
        }
    }
}

#[cfg(test)]
mod test {
    use super::ExpectedValues;
    use super::Values;

    #[test]
    fn value_prints() {
        let v = Values::Numeric(0.0);
        let vb = Values::Bool(false);
        let vs = Values::String("stringy".to_string());
        assert_eq!(v.to_string(), "0");
        assert_eq!(vb.to_string(), "false");
        assert_eq!(vs.to_string(), "stringy");
    }

    #[test]
    fn expected_value_prints() {
        let v = ExpectedValues::Numeric(1.1);
        let vb = ExpectedValues::Bool(true);
        let vs = ExpectedValues::String("stringy".to_string());
        assert_eq!(v.to_string(), "1.1");
        assert_eq!(vb.to_string(), "true");
        assert_eq!(vs.to_string(), "stringy");
    }

    #[test]
    fn value_conversion() {
        let orig = vec![
            Values::Numeric(0.0),
            Values::String("one two three".to_string()),
            Values::Bool(true),
        ];
        for v in orig {
            let e: ExpectedValues = v.into();
            let _l: Values = e.into();
        }
    }
}
