pub(crate) async fn store(connection: zbus::Connection) -> Result<(), zbus::Error> {
    const VERSIONS: [&str; 2] = ["mqttbridge.version", "modio.mqttbridge.version"];

    let ipc = fsipc::legacy::fsipcProxy::builder(&connection)
        .build()
        .await?;
    let logger1 = fsipc::logger1::Logger1Proxy::builder(&connection)
        .build()
        .await?;

    for key in VERSIONS {
        ipc.store(key, env!("CARGO_PKG_VERSION")).await?;
        logger1
            .set_metadata_name(key, "MQTT Bridge: Version number")
            .await?;
    }
    Ok(())
}
