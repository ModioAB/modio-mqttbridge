use zbus::zvariant;

pub fn zvariant_to_string(value: &zvariant::Value<'_>) -> Option<String> {
    use zbus::zvariant::Value::{Bool, Str, F64, I16, I32, I64, U16, U32, U64, U8};
    match value {
        U16(v) => Some(format!("{v}")),
        U32(v) => Some(format!("{v}")),
        U64(v) => Some(format!("{v}")),
        U8(v) => Some(format!("{v}")),
        I16(v) => Some(format!("{v}")),
        I32(v) => Some(format!("{v}")),
        I64(v) => Some(format!("{v}")),
        F64(v) => Some(format!("{v}")),
        Str(v) => Some(format!("{v}")),
        Bool(v) => Some(format!("{}", u32::from(*v))),
        _ => None,
    }
}
