// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: AGPL-3.0-or-later
use log::{debug, error, info, warn};
use rumqttc::{self, EventLoop, Packet};
use tokio::sync::mpsc;

use crate::Transaction;

fn filter_transaction(prefix: &str, indata: rumqttc::Packet) -> Option<Transaction> {
    // Check if it really was meant for the one subject we care about
    match indata {
        Packet::Publish(inner) if inner.topic.starts_with(prefix) => {
            debug!(target: "filter_transaction", "topic={}, payload={:?}",
                &inner.topic, &inner.payload
            );
            let log_err = |msg| warn!(target: "filter_transaction", "Error decoding, msg={}", msg);
            // Log any error, then turn it into an Option
            Transaction::from_bytes(&inner.payload)
                .map_err(log_err)
                .ok()
        }
        // Packet wasn't part of our topic
        Packet::Publish(other) => {
            warn!(target: "filter_transaction",
                  "Decoded, prefix mismatch,  prefix={},  topic={},  payload={:?}",
                  &prefix, &other.topic, &other.payload
            );
            None
        }
        // Some other kind of packet
        _ => None,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use bytes::Bytes;
    use rumqttc::QoS;

    const GOOD: &str = r#"{"n":"28.000000000000","expected":"21","v":22,"token":"ac06f54fb71240058e7adff71b8ca270"}"#;
    #[test]
    fn test_decode_packet_works() {
        let payload = Bytes::from(GOOD);
        let inner = rumqttc::v4::Publish::from_bytes("modio/change", QoS::ExactlyOnce, payload);
        let packet = Packet::Publish(inner);
        assert!(
            filter_transaction("modio/change", packet).is_some(),
            "Should work as the json is valid and topics match"
        );
    }

    #[test]
    fn test_decode_packet_subtree() {
        let payload = Bytes::from(GOOD);
        let inner =
            rumqttc::v4::Publish::from_bytes("modio/change/sub/tree", QoS::ExactlyOnce, payload);
        let packet = Packet::Publish(inner);
        assert!(
            filter_transaction("modio/change", packet).is_some(),
            "Should work as the json is valid and topics match"
        );
    }

    #[test]
    fn test_decode_packet_fails_invalid_json() {
        // Payload lacks token
        let payload =
            Bytes::from(r#"{"n":"28.000000000000","expected":"21","v":22,"extra":1636732400}"#);

        let inner = rumqttc::v4::Publish::from_bytes("modio/change", QoS::ExactlyOnce, payload);
        let packet = Packet::Publish(inner);
        assert!(
            filter_transaction("modio/change", packet).is_none(),
            "Should fail as the payload lacks a token"
        );
    }

    #[test]
    fn test_decode_packet_topic_mismatch() {
        let payload = Bytes::from(GOOD);
        let inner = rumqttc::v4::Publish::from_bytes("foo/bar", QoS::ExactlyOnce, payload);
        let packet = Packet::Publish(inner);
        assert!(
            filter_transaction("foo/baz", packet).is_none(),
            "Should not decode as topic mismatches"
        );
    }
}

pub(crate) struct EventPollerActor {
    eventloop: EventLoop,
    transaction_prefix: String,
    tx: mpsc::Sender<Transaction>,
}

impl EventPollerActor {
    pub fn new(
        eventloop: EventLoop,
        transaction_prefix: String,
        tx: mpsc::Sender<Transaction>,
    ) -> EventPollerActor {
        EventPollerActor {
            eventloop,
            transaction_prefix,
            tx,
        }
    }

    async fn handle_event(&mut self, event: rumqttc::Event) {
        use rumqttc::Event;
        match event {
            Event::Incoming(indata) => {
                // Useful for debugging, but can cause it to be too spammy.
                // trace!(target: "eventpoller", "In< {:?}", &indata);
                if let Some(data) = filter_transaction(&self.transaction_prefix, indata) {
                    if let Err(msg) = self.tx.send(data).await {
                        error!(target: "eventpoller", "Error passing transaction to ipc: {}", msg);
                    }
                }
            }
            Event::Outgoing(_outdata) => {
                // Commented out as it will write on every packet.
                // Useful for debugging, but causes trace level to be too spammy
                // trace!(target: "eventpoller", "Out> {:?}", _outdata);
                //
            }
        }
    }
}

pub(crate) async fn run_mqtt_eventpoller(
    mut actor: EventPollerActor,
) -> Result<(), rumqttc::ConnectionError> {
    info!(target: "eventpoller",
        "Polling for published packets, caring about prefix={}",
        &actor.transaction_prefix
    );
    loop {
        let event = actor.eventloop.poll().await?;
        actor.handle_event(event).await;
    }
}
