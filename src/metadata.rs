// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: AGPL-3.0-or-later
use fsipc::logger1::ValueMap;
use serde::Serialize;
use std::convert::From;
use std::fmt;

impl From<fsipc::logger1::Metadata> for Metadata {
    fn from(item: fsipc::logger1::Metadata) -> Self {
        Metadata {
            n: item.n,
            u: item.u,
            name: item.name,
            description: item.description,
            value_map: item.value_map,
            mode: item.mode.map(SensorMode::from),
        }
    }
}

#[derive(Debug, PartialEq, Serialize)]
pub(crate) enum SensorMode {
    #[serde(rename = "ro")]
    ReadOnly,
    #[serde(rename = "rw")]
    ReadWrite,
    #[serde(rename = "wo")]
    WriteOnly,
}

impl From<fsipc::logger1::SensorMode> for SensorMode {
    fn from(item: fsipc::logger1::SensorMode) -> SensorMode {
        match item {
            fsipc::logger1::SensorMode::ReadOnly => SensorMode::ReadOnly,
            fsipc::logger1::SensorMode::ReadWrite => SensorMode::ReadWrite,
            fsipc::logger1::SensorMode::WriteOnly => SensorMode::WriteOnly,
        }
    }
}

#[derive(Debug, Serialize)]
pub(crate) struct Metadata {
    pub n: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub u: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub value_map: Option<ValueMap>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub mode: Option<SensorMode>,
}

// Implementation of Display gives us a to_string  option.
// We use json for this as a convenience
impl fmt::Display for Metadata {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = serde_json::to_string(&self).map_err(|_| std::fmt::Error::default())?;
        write!(f, "{s}")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_serialize_mode() {
        let meta = Metadata {
            n: String::from("modio.test.test"),
            u: None,
            name: None,
            description: None,
            value_map: None,
            mode: Some(SensorMode::ReadWrite),
        };
        let res = serde_json::to_string(&meta).unwrap();
        let expected = r#"{"n":"modio.test.test","mode":"rw"}"#;
        assert_eq!(res, expected);
    }
}
