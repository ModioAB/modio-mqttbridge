// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: AGPL-3.0-or-later
/// This file is unused but was representing what we WANTED to do.
/// However, as the senml crates werent up to the task when I started this, I am only leaving the
/// traces here so you can find them later and ignore them as being out of date.
use super::*;
use serde_json::Result;

// See https://datatracker.ietf.org/doc/html/rfc8428#section-11
#[cfg(test)]
const SENML_CDDL: &str = r#"SenML-Pack = [1* record]
  record = {
    ? bn => tstr,        ; Base Name
    ? bt => numeric,     ; Base Time
    ? bu => tstr,        ; Base Units
    ? bv => numeric,     ; Base Value
    ? bs => numeric,     ; Base Sum
    ? bver => uint,      ; Base Version
    ? n => tstr,        ; Name
    ? u => tstr,        ; Units
    ? s => numeric,     ; Sum
    ? t => numeric,     ; Time
    ? ut => numeric,    ; Update Time
    ? ( v => numeric // ; Numeric Value
        vs => tstr //   ; String Value
        vb => bool //   ; Boolean Value
        vd => binary-value ) ; Data Value
    * key-value-pair
  }

  ; now define the generic versions
  key-value-pair = ( label => value )

  label = non-b-label / b-label
  non-b-label = tstr .regexp  "[A-Zac-z0-9][-_:.A-Za-z0-9]*" / uint
  b-label = tstr .regexp  "b[-_:.A-Za-z0-9]+" / nint

  value = tstr / binary-value / numeric / bool
  numeric = number / decfrac
"#;

#[test]
#[ignore]
/// Broken test atm. cddl crate doesn't like json
fn test_cddl_parse() {
    use cddl::validate_json_from_str;
    let row = r#"[{"bn": "urn:dev:mac:ffffaaaa0000eeee:", "n": "modio.test.test", "vs": "abc123", "t": 1628546838}] "#;
    println!("Row is :{}", row);
    let res = validate_json_from_str(SENML_CDDL, row);
    res.expect("Something wrong");
}

#[test]
#[ignore]
/// Broken test atm. cddl crate doesn't like json
fn test_json_roundtrip() -> Result<()> {
    use cddl::validate_json_from_str;
    let datum = LimitedSenML::make_one(
        "urn:dev:mac:ffffaaaa0000eeee:",
        "modio.test.test",
        "abc123",
        1_628_546_838.0,
        None,
    );
    let data: std::vec::Vec<LimitedSenML> = vec![datum];
    // Serialize it to a JSON string.
    let row = serde_json::to_string(&data)?;
    println!("Row is :{:?}", row);
    validate_json_from_str(SENML_CDDL, &row).expect("test failed");
    Ok(())
}
