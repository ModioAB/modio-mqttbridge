// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: AGPL-3.0-or-later
use std::path::PathBuf;
use std::sync::Arc;

use argh::FromArgs;
use log::{error, info};
use rumqttc::{self, AsyncClient};

mod broadcast;
mod matches;
mod metadata;
mod modioipc;
mod senml_limited;
mod submit1;
mod transaction;
mod values;
mod version;

use crate::matches::Matches;
use transaction::Transaction;

#[derive(FromArgs, Debug)]
/// Command line args for modio-mqttc
///
struct CommandLine {
    /// config file
    ///
    /// Path to the configuration file.
    /// Technically not used, but serves as a flag.
    #[argh(option, short = 'c')]
    config: Option<PathBuf>,

    /// use session bus
    ///
    /// By default we use the DBus System bus, add this flag to use the Session bus, or "user" bus
    /// instead. Mainly useful when developing
    #[argh(switch)]
    session: bool,

    /// verbosity
    ///
    /// Add more -v -v -v in order to get a higher log level
    #[argh(switch, short = 'v')]
    verbose: u32,

    /// tls-dir
    ///
    /// Path to directory where ca.crt,  client.crt and client.key are located.
    /// If unset, uses env var STATE_DIRECTORY or a default
    #[argh(option, default = "default_tls_dir()")]
    tls_dir: PathBuf,
}

const fn verbosity(verbose: u32) -> log::LevelFilter {
    match verbose {
        0 => log::LevelFilter::Error,
        1 => log::LevelFilter::Warn,
        2 => log::LevelFilter::Info,
        3 => log::LevelFilter::Debug,
        _ => log::LevelFilter::Trace,
    }
}

/// Get the default TLS directory
///
/// This checks the environment variable `STATE_DIRECTORY` to use for the dir where TLS certificates
/// and keys are stored.
/// The first such path is used if multiple exist.
///
/// If the env var is not set, the default of `/var/lib/modio-mqttbridge` is returned.
fn default_tls_dir() -> PathBuf {
    use std::env;
    // Set by systemd service file StateDirectory=
    let key = "STATE_DIRECTORY";
    let default = PathBuf::from("/var/lib/modio-mqttbridge");

    //
    match env::var_os(key) {
        Some(paths) => match env::split_paths(&paths).next() {
            Some(path) => path,
            None => default,
        },
        None => default,
    }
}

mod tls {
    use log::info;

    use std::path::PathBuf;

    async fn load_file(path: PathBuf) -> Result<Vec<u8>, std::io::Error> {
        use tokio::fs::File;
        use tokio::io::AsyncReadExt;
        let mut file = File::open(path).await?;
        let mut content = vec![];
        file.read_to_end(&mut content).await?;
        Ok(content)
    }

    pub async fn make_transport(
        ca: PathBuf,
        key: PathBuf,
        cert: PathBuf,
    ) -> Result<rumqttc::Transport, std::io::Error> {
        info!("Loading CA from {:?}", ca);
        let ca_data = load_file(ca).await?;
        info!("Loading client cert from {:?}", cert);
        let cert_data = load_file(cert).await?;
        info!("Loading client key as RSA from {:?}", key);
        let key_data = load_file(key).await?;
        let rsa_key = rumqttc::Key::RSA(key_data);
        let tls_config = rumqttc::TlsConfiguration::Simple {
            ca: ca_data,
            alpn: None,
            client_auth: Some((cert_data, rsa_key)),
        };
        let result = rumqttc::Transport::Tls(tls_config);
        Ok(result)
    }
}

mod configfile {
    use super::tls;
    use log::{info, warn};
    use rumqttc::{self, MqttOptions};
    use std::error::Error;
    use std::path::{Path, PathBuf};
    use std::time::Duration;

    #[derive(Debug)]
    pub struct ConfigFile {
        pub name: String,
        pub host: String,
        pub prefix: String,
        pub port: u16,
        pub tls: bool,
        pub timeout: u64,
        pub key_prefixes: Vec<String>,
    }

    impl ConfigFile {
        pub fn default() -> Self {
            Self {
                name: "modio-mqttc".to_string(),
                host: "localhost".to_string(),
                prefix: "modio/poc".to_string(),
                port: 1883,
                tls: false,
                timeout: 57,
                key_prefixes: Vec::<String>::new(),
            }
        }
        pub fn from_file(config_path: PathBuf) -> Result<Self, Box<dyn Error>> {
            const SECTION: &str = "mqttbridge";
            info!("Loading configuration from {:?}", config_path);
            let mut config = configparser::ini::Ini::new();
            config.load(config_path)?;

            // The "configparser"  lib uses Strings for errors rather than error types.
            // Therefore, we use "ok_or" here with strings, because that keeps the same error type.
            let name = config
                .get(SECTION, "name")
                .ok_or("[mqttbridge]: Missing name=")?;
            let host = config
                .get(SECTION, "host")
                .ok_or("[mqttbridge]: Missing host=")?;
            let prefix = config
                .get(SECTION, "prefix")
                .ok_or("[mqttbridge]: Missing prefix=")?;
            let port_s = config
                .get(SECTION, "port")
                .ok_or("[mqttbridge]: Missing port=")?;
            let tls = config
                .getbool(SECTION, "tls")
                .unwrap_or(Some(false))
                .ok_or("[mqttbridge]: Missing tls=")?;
            let key_prefix = config.get(SECTION, "key_prefix").unwrap_or_default();
            let timeout_s = config
                .get(SECTION, "timeout")
                .unwrap_or_else(|| String::from("57"));

            let port = port_s.parse::<u16>()?;
            let timeout = timeout_s.parse::<u64>()?;
            let key_prefixes: Vec<String> =
                key_prefix.split_whitespace().map(String::from).collect();
            let res = Self {
                name,
                host,
                prefix,
                port,
                tls,
                timeout,
                key_prefixes,
            };
            Ok(res)
        }
        /// Collapse the parsed configFile into an Mqttoptions, and return the Prefix contained
        /// within, as well as a vec of key prefixes
        ///
        /// Takes the path to a tls directory to load certs from.
        pub async fn make_mqttoptions(
            &self,
            tls_dir: &Path,
        ) -> Result<MqttOptions, Box<dyn Error>> {
            // Since the only thing left in the config file after making MQTT options is the
            // prefix, this method returns both, thus completly consuming our struct.
            let mut mqttoptions = MqttOptions::new(&self.name, &self.host, self.port);
            if self.tls {
                let ca = tls_dir.join("ca.crt");
                let key = tls_dir.join("client.key");
                let cert = tls_dir.join("client.crt");
                let transport = tls::make_transport(ca, key, cert).await?;
                mqttoptions.set_transport(transport);
            } else {
                warn!("Not using TLS");
            }
            let keep_alive = Duration::from_secs(5);
            mqttoptions.set_keep_alive(keep_alive);
            Ok(mqttoptions)
        }
    }
}

#[tokio::main(worker_threads = 1)]
async fn main() {
    // Parse commandline options
    let commandline: CommandLine = argh::from_env();

    // Set up logging
    env_logger::Builder::new()
        .filter_level(verbosity(commandline.verbose))
        .format_timestamp_secs()
        .write_style(env_logger::WriteStyle::Auto)
        .parse_default_env()
        .init();

    log::debug!("env_logger, color, timestamp seconds");
    println!("{} {}", env!("CARGO_BIN_NAME"), env!("CARGO_PKG_VERSION"));

    let (tran_tx, tran_rx) = tokio::sync::mpsc::channel::<Transaction>(10);

    info!("CommandLine: {:#?}", commandline);

    // Start a little block for config file parsing and related
    // build data structures and allocate memory, files, and such.
    let (mqttoptions, topic_prefix, desired_keys, timeout) = {
        let config = if let Some(config_file) = commandline.config {
            configfile::ConfigFile::from_file(config_file).expect("Error loading config file")
        } else {
            configfile::ConfigFile::default()
        };
        info!("Configuration parsed as {:?}", config);

        let mqttoptions = config
            .make_mqttoptions(&commandline.tls_dir)
            .await
            .expect("Failed to load certificates");

        let (topic_prefix, key_prefixes) = { (config.prefix, config.key_prefixes) };
        let matches = Arc::new(Matches::new(key_prefixes));
        (mqttoptions, topic_prefix, matches, config.timeout)
    };
    // We now have mqttoptions (tls certs, hosts resolved, and so on?)
    // our "prefix" and an Matches data-structure that list any desired items

    let (client, mut eventloop) = AsyncClient::new(mqttoptions, 10);
    eventloop.network_options.set_connection_timeout(timeout);

    // We don't expect a hierarchy under /change,  so we do not apply /# for that
    // let subscribe_prefix = format!("{}/change/#", topic_prefix);
    let subscribe_prefix = format!("{topic_prefix}/change/legacy");
    client
        .subscribe(&subscribe_prefix, rumqttc::QoS::AtMostOnce)
        .await
        .expect("Failed to subscribe to our incoming prefix");

    let connection = modioipc::make_connection(commandline.session)
        .await
        .expect("Failed to connect to dbus");

    // Metadata queries should not be recursive on the same connection that we want signals from,
    // while it may be okay at times, it can cause our replies to be lost in an disturbing way as
    // signals can overwrite the buffer.
    let meta_connection = modioipc::make_connection(commandline.session)
        .await
        .expect("Failed to connect to dbus a second time");

    let mqtt_actor = broadcast::EventPollerActor::new(eventloop, subscribe_prefix, tran_tx);
    let mqtt_read_loop = tokio::task::spawn(async move {
        info!("Spawning mqttc eventloop poller!");
        broadcast::run_mqtt_eventpoller(mqtt_actor)
            .await
            .expect("MQTT event poller failed. Network issue?");
    });

    // Publish to our root topic,
    //     with a higher than normal QoS,
    //     marked as sticky,
    //     an empty json object.
    client
        .publish(&topic_prefix, rumqttc::QoS::AtLeastOnce, true, "{}")
        .await
        .expect("Could not publish life annoucement");

    // Set up an actor for metadata.
    // This is responsible for querying metadata from dbus, but not in the line of processing
    // metrics.
    // - It listens to dbus for metadata updated signals and refreshes internal cache if needed
    // - it listens for transactions on dbus to publish all MQTT data
    // - it listens for requests from seen new values to publish the cached metadata in the
    //   background
    let meta_actor = modioipc::meta::Meta::new(
        meta_connection,
        client.clone(),
        topic_prefix.clone(),
        desired_keys.clone(),
    );
    // Metadata client that the data value processing uses to query cache and request metadata
    // updates
    let meta_client = meta_actor.client();

    let meta_loop = tokio::task::spawn(async move {
        info!("Spawning Task modioipc::meta_loop");
        modioipc::run_meta_actor(meta_actor)
            .await
            .expect("Something is wrong with the ipc for metadata?");
    });

    // Processing for the metadata queries
    let signal_actor = modioipc::SignalActor::new(
        connection.clone(),
        client.clone(),
        desired_keys,
        topic_prefix,
        meta_client,
    )
    .await
    .expect("Failed to initialze the signal processor");

    let signal_loop = tokio::task::spawn(async move {
        info!("Spawning Task modioipc::signal_loop");
        modioipc::run_signal_actor(signal_actor)
            .await
            .expect("Something is wrong with the ipc for signals?");
    });
    let change_actor = modioipc::TransactionActor::new(connection.clone(), tran_rx);
    let change_loop = tokio::task::spawn(async move {
        info!("Spawning Task modioipc::transaction_loop");
        modioipc::run_transaction_actor(change_actor)
            .await
            .expect("Something is wrong with the ipc for transactions?");
    });

    if let Err(err) = version::store(connection).await {
        error!("Failed to set version: {err}");
    }

    println!("Background tasks loaded, waiting forever");
    tokio::try_join!(change_loop, signal_loop, mqtt_read_loop, meta_loop)
        .expect("Error in some subprocess");
}
