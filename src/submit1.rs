use fsipc::logger1::Metadata;
use zbus::dbus_proxy;

#[dbus_proxy(
    interface = "se.modio.logger.Submit1",
    default_service = "se.modio.logger",
    default_path = "/se/modio/logger/customer"
)]
trait Submit1 {
    /// GetMetadata method
    fn get_all_metadata(&self) -> zbus::Result<Vec<Metadata>>;
}
