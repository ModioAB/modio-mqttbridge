// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: AGPL-3.0-or-later
use rumqttc::{AsyncClient, QoS};
use std::result::Result;
use std::sync::Arc;
use std::time::Duration;

use futures_util::stream::StreamExt;
use log::{debug, info};
use tokio::sync::mpsc;

use crate::Transaction;

use fsipc::legacy::fsipcProxy;
use fsipc::logger1::Logger1Proxy;

use crate::matches::Matches;
use crate::senml_limited::SenML;
use crate::values::zvariant_to_string;

const METADATA_KEY: &str = "mqttbridge.metadata.publish";
pub async fn make_connection(session: bool) -> Result<zbus::Connection, zbus::Error> {
    let connection = if session {
        debug!("Connecting to session bus");
        zbus::ConnectionBuilder::session()?
            .max_queued(1200)
            .build()
            .await?
    } else {
        debug!("Connecting to system bus");
        zbus::ConnectionBuilder::system()?
            .max_queued(1200)
            .build()
            .await?
    };
    Ok(connection)
}

pub(crate) mod meta {
    use super::METADATA_KEY;
    use crate::matches::Matches;
    use crate::metadata::Metadata;
    use crate::submit1::Submit1Proxy;
    use fsipc::legacy::fsipcProxy;
    use fsipc::logger1::{Logger1Proxy, MetadataUpdated};
    use futures_util::stream::StreamExt;
    use log::{debug, error, info, warn};
    use rumqttc::{AsyncClient, QoS};
    use std::collections::HashMap;
    use std::sync::{Arc, Mutex};
    use std::time::Duration;
    use tokio::sync::mpsc;

    fn placeholder(key: &str) -> Metadata {
        Metadata {
            n: key.to_string(),
            u: None,
            name: None,
            description: None,
            value_map: None,
            mode: None,
        }
    }

    #[derive(Clone)]
    pub(crate) struct MetaClient {
        pub cache: Arc<Mutex<HashMap<String, Metadata>>>,
        pub sender: mpsc::Sender<String>,
    }
    pub struct NameUnit(pub Option<String>, pub Option<String>);

    impl MetaClient {
        // Check if key exists in cache, sync function as it takes a Mutex lock
        fn cache_contains_key(&self, key: &str) -> bool {
            let cache = self.cache.lock().expect("Mutex poisoned");
            cache.contains_key(key)
        }

        pub fn cache_len(&self) -> usize {
            let cache = self.cache.lock().expect("Mutex poisoned");
            cache.len()
        }

        // Get metadata u and name for key, sync function as it takes a Mutex lock
        // Only grabs some parts so we do not accidentally copy Metadata all over the place.
        fn cache_get_name_unit(&self, key: &str) -> NameUnit {
            let cache = self.cache.lock().expect("Mutex poisoned");
            if let Some(val) = cache.get(key) {
                NameUnit(val.name.clone(), val.u.clone())
            } else {
                NameUnit(None, None)
            }
        }

        // Request the metadata for key to be refreshed
        fn refresh_key(&self, key: &str) -> Result<(), MError> {
            use tokio::sync::mpsc::error::TrySendError;
            match self.sender.try_send(key.into()) {
                Err(TrySendError::Full(e)) => {
                    info!(target: "metadata", "Channel for metadata lookup is full, key={}, err={}", key, e);
                    Ok(())
                }
                Err(TrySendError::Closed(e)) => {
                    error!(target: "metadata", "Channel for metadata lookup is closed. Terminating. err={}", e);
                    Err(MError::Shutdown)
                }
                _ => Ok(()),
            }
        }

        // Query metadata for key, if not found, request it to be filled in.
        // Doesnt return a full Metadata object as we do not need that, only Name & Unit to save us
        // from copying text-data we do not care about.
        pub fn query(&self, key: &str) -> Result<NameUnit, MError> {
            // if we have it cached, do not request more expensive round-trips to fill in metadata
            // from logger.
            // This is inherently racy, but that is ok
            if !self.cache_contains_key(key) {
                self.refresh_key(key)?;
            }
            let res = self.cache_get_name_unit(key);
            Ok(res)
        }
    }

    pub(crate) struct Meta {
        cache: Arc<Mutex<HashMap<String, Metadata>>>,
        matches: Arc<Matches>,
        connection: zbus::Connection,
        client: AsyncClient,
        prefix: String,
        sender: mpsc::Sender<String>,
        pub(crate) receiver: mpsc::Receiver<String>,
    }

    impl Meta {
        pub fn new(
            connection: zbus::Connection,
            client: AsyncClient,
            prefix: String,
            matches: Arc<Matches>,
        ) -> Meta {
            // The channel is small as it's better to re-attempt with metadata due to full channel
            // than it is to overload the logger to death
            let (sender, receiver) = mpsc::channel(17);
            let cache = HashMap::<String, Metadata>::new();
            let cache = Arc::new(Mutex::new(cache));

            Meta {
                cache,
                matches,
                connection,
                client,
                prefix,
                sender,
                receiver,
            }
        }
        pub fn client(&self) -> MetaClient {
            let cache = self.cache.clone();
            let sender = self.sender.clone();
            MetaClient { cache, sender }
        }

        /// Broadcast an item to the MQTT bus.
        async fn broadcast_item(&self, key: &str, data: String) -> Result<(), MError> {
            if !self.matches.key_matches(key) {
                return Ok(());
            }
            let topic = format!("{}/metadata/{}", &self.prefix, key.replace('.', "/"));
            debug!(target: "metadata", "about to publish to {} = {}", &topic, &data);
            self.client
                .publish(topic, QoS::AtMostOnce, false, data)
                .await?;
            Ok(())
        }

        // Check if key exists in cache, sync function as it takes a Mutex lock
        fn cache_contains_key(&self, key: &str) -> bool {
            let cache = self.cache.lock().expect("Mutex poisoned");
            cache.contains_key(key)
        }

        pub fn cache_len(&self) -> usize {
            let cache = self.cache.lock().expect("Mutex poisoned");
            cache.len()
        }

        fn cache_keys(&self) -> Vec<String> {
            let cache = self.cache.lock().expect("Mutex poisoned");
            cache.keys().map(ToString::to_string).collect()
        }

        fn cache_insert(&self, data: Metadata) {
            let key = data.n.to_string();
            let mut cache = self.cache.lock().expect("Mutex poisoned");
            cache.insert(key, data);
        }

        // Helper for when we want Metadata as a String, for mqtt send case
        fn cache_get_string(&self, key: &str) -> Option<String> {
            let cache = self.cache.lock().expect("Mutex poisoned");
            cache.get(key).map(ToString::to_string)
        }

        /// Broadcast all items to the MQTT bus.
        pub async fn broadcast_all(&self) -> Result<(), MError> {
            let num = self.cache_len();
            info!(target: "metadata", "About to publish all metadata, count={}", num);
            let keys = self.cache_keys();
            for key in keys {
                if self.matches.key_matches(&key) {
                    if let Some(data) = self.cache_get_string(&key) {
                        let topic = format!("{}/metadata/{}", &self.prefix, key.replace('.', "/"));
                        self.client
                            .publish(topic, QoS::AtMostOnce, false, data)
                            .await?;
                    }
                }
            }
            Ok(())
        }

        async fn ipc(&self) -> Result<Logger1Proxy, zbus::Error> {
            let ipc = Logger1Proxy::builder(&self.connection)
                .destination("se.modio.logger")?
                .path("/se/modio/logger")?
                .build()
                .await?;
            Ok(ipc)
        }

        async fn bus_query(&self, key: &str) -> Result<Metadata, MError> {
            debug!(target: "metadata", "Querying modio-logger for metadata on key={}", key);
            let ipc = self.ipc().await?;
            match ipc.get_metadata(key).await {
                Ok(data) => {
                    let data = Metadata::from(data);
                    Ok(data)
                }
                Err(e) => Err(MError::from(e)),
            }
        }

        async fn fetch_all_metadata(&self) -> Result<(), MError> {
            info!(target: "metadata", "Querying modio-logger for all metadata.");
            let ipc = Submit1Proxy::builder(&self.connection).build().await?;
            let mut dataset = ipc.get_all_metadata().await?;
            let count = dataset.len();
            for meta in dataset.drain(..).map(Metadata::from) {
                if self.matches.key_matches(&meta.n) {
                    self.cache_insert(meta);
                }
            }
            info!(target: "metadata", "Pre-filled count={count} metadata items.");
            Ok(())
        }

        async fn poll_one_inner(&mut self, key: &str) -> Result<(), MError> {
            match self.bus_query(key).await {
                Ok(data) => {
                    debug!(target: "metadata", "Got metadata for key={} metadata={:?}", key, data);
                    let broadcast_data = data.to_string(); // Convert here as we want to insert it
                                                           // to cache before we do network
                                                           // traffic.
                    self.cache_insert(data);
                    self.broadcast_item(key, broadcast_data).await?;
                }
                Err(MError::NotFound) => {
                    info!(target: "metadata", "No metadata for key={}", key);
                    let data = placeholder(key);
                    self.cache_insert(data);
                }
                Err(other) => {
                    error!(target: "metadata", "Error fetching metadata for key={} err={}", key, other);
                    return Err(other);
                }
            }
            Ok(())
        }

        pub(crate) async fn poll_one(&mut self, key: String) -> Result<(), zbus::Error> {
            const MAX_WAIT: Duration = Duration::from_millis(100);
            let future = self.poll_one_inner(&key);
            if tokio::time::timeout(MAX_WAIT, future).await.is_err() {
                // Timeout error happened.
                // This is not an error we failure from, just continue.
                warn!(target: "metadata", "No response from modio-logger when querying key={}, wait={}", key, MAX_WAIT.as_secs_f32());
            }
            Ok(())
        }

        async fn transaction_handle_once(&self) -> Result<(), MError> {
            let ipc = fsipcProxy::builder(&self.connection).build().await?;
            info!(target: "ipc_transaction", "Querying for pending transactions for mqttbridge");
            let trans = ipc.transaction_get(METADATA_KEY).await?;
            for trn in trans {
                if trn.key == METADATA_KEY {
                    info!(target: "ipc_transaction", "Broadcasting all metadata");
                    self.broadcast_all().await?;
                    info!(target: "ipc_transaction", "Passing transaction: {}", trn);
                    ipc.transaction_pass(trn.t_id).await?;
                    ipc.store(METADATA_KEY, "0").await?;
                } else {
                    info!(target: "ipc_transaction", "Failing transaction: {}", trn);
                    ipc.transaction_fail(trn.t_id).await?;
                }
            }
            Ok(())
        }
        async fn process_transaction_signal(
            &mut self,
            v: fsipc::legacy::TransactionAdded,
        ) -> Result<(), MError> {
            use fsipc::legacy::TransactionAddedArgs as Args;
            match v.args()? {
                Args {
                    key: METADATA_KEY, ..
                } => self.transaction_handle_once().await,
                /* Transaction was not for our key.
                Might be for another service entirely, don't log it.  */
                _ => Ok(()),
            }
        }

        async fn process_metadata_signal(&mut self, v: MetadataUpdated) -> Result<(), MError> {
            let args = v.args()?;
            // Only refresh the key if we have it already.
            // Otherwise we would maybe poulate data too often, or re-broadcast after every call
            // to the logger
            if self.cache_contains_key(args.key) {
                self.sender.send(args.key.into()).await?;
            }
            Ok(())
        }
    }

    #[derive(Debug, thiserror::Error)]
    /// Since the dbus layer passes the error type through internal data, this is a bit reduced.
    pub enum MError {
        #[error("Not Found")]
        NotFound,
        #[error("MQTT metadata relate")]
        Mqtt(#[from] rumqttc::ClientError),
        #[error("Other Error")]
        Other(zbus::Error),
        #[error("Channel Error")]
        Channel(#[from] tokio::sync::mpsc::error::SendError<String>),
        #[error("Shut down")]
        Shutdown,
    }

    impl From<zbus::Error> for MError {
        fn from(v: zbus::Error) -> MError {
            match v {
                zbus::Error::MethodError(ref errname, _, _) => match errname.as_str() {
                    "se.modio.logger.fsipc.NotFound" => MError::NotFound,
                    _ => MError::Other(v),
                },
                _ => MError::Other(v),
            }
        }
    }

    pub(crate) async fn run_meta_actor(mut actor: Meta) -> Result<(), MError> {
        if let Err(e) = actor.fetch_all_metadata().await {
            error!(target: "metadata", "Failed to query all metadata from logger. err={}", e);
        }

        let ipc = fsipcProxy::builder(&actor.connection).build().await?;
        let mut transactions = ipc.receive_transaction_added().await?;
        let logger1 = Logger1Proxy::builder(&actor.connection).build().await?;
        let mut metadata = logger1.receive_metadata_updated().await?;
        info!(target: "metadata", "Metadata poller ready, awaiting questions");

        loop {
            tokio::select! {
                Some(trn) = transactions.next() => {
                    // If we get a signal to broadcast all metadata, we should act upon it.
                    actor.process_transaction_signal(trn).await?;
                },
                // If we get a new key in our queue to fetch for metadata.
                Some(query) = actor.receiver.recv() => {
                    actor.poll_one(query).await?;
                },
                // Logger says we have new metadata and should bust our cache
                Some(met) = metadata.next() => {
                    actor.process_metadata_signal(met).await?;
                },
                else => { break },
            };
        }
        panic!("run_metadata_actor: No more messages to process?")
    }
}
pub(crate) use meta::run_meta_actor;

async fn update_metadata_names(connection: &zbus::Connection) -> Result<(), zbus::Error> {
    use fsipc::logger1::SensorMode;
    let ipc = Logger1Proxy::builder(connection)
        .destination("se.modio.logger")?
        .path("/se/modio/logger")?
        .build()
        .await?;

    ipc.set_metadata_name(METADATA_KEY, "MQTT Bridge: Metadata publish")
        .await?;
    ipc.set_metadata_description(
        METADATA_KEY,
        "Write to this value to re-publish metadata to mqtt",
    )
    .await?;
    ipc.set_metadata_mode(METADATA_KEY, SensorMode::ReadWrite)
        .await?;
    ipc.set_metadata_name("modio.mqttbridge.store", "MQTT Bridge: Store loop active")
        .await?;
    ipc.set_metadata_name(
        "modio.mqttbridge.transaction",
        "MQTT Bridge: Transaction loop active",
    )
    .await?;
    Ok(())
}

pub async fn get_bn(ipc: &fsipcProxy<'_>) -> Result<String, zbus::Error> {
    let boxid: String = ipc.get_boxid().await?;
    info!(target: "signal_loop", "Got boxid: {}", boxid);
    let bn: String = format!("urn:dev:mac:{boxid}:");
    Ok(bn)
}

pub(crate) struct SignalActor {
    connection: zbus::Connection,
    client: AsyncClient,
    matches: Arc<Matches>,
    prefix: String,
    bn: String,
    metrics_count: usize,
    meta: meta::MetaClient,
}

impl SignalActor {
    pub async fn new(
        connection: zbus::Connection,
        client: AsyncClient,
        matches: Arc<Matches>,
        prefix: String,
        meta: meta::MetaClient,
    ) -> Result<SignalActor, zbus::Error> {
        let ipc = fsipcProxy::builder(&connection).build().await?;
        let bn = get_bn(&ipc).await?;

        Ok(SignalActor {
            connection,
            client,
            matches,
            prefix,
            bn,
            metrics_count: 0,
            meta,
        })
    }

    async fn process_one_metric(
        &mut self,
        key: &str,
        value: &str,
        ts: f64,
    ) -> Result<(), meta::MError> {
        if !self.matches.key_matches(key) {
            debug!(target: "signal_loop", "Ignored due to prefix filter: {}", key);
            return Ok(());
        }
        let meta_u_n = self.meta.query(key)?;
        let datum = SenML::make_one(
            &self.bn,
            key,
            value,
            ts,
            meta_u_n.0.as_deref(),
            meta_u_n.1.as_deref(),
        );
        let topic = datum.make_topic(&self.prefix);
        let to_xmit = datum.to_string();

        debug!(target: "signal_loop", "about to publish to {} = {}", &topic, &to_xmit);
        self.metrics_count += 1;
        self.client
            .publish(topic, QoS::AtMostOnce, false, to_xmit)
            .await?;
        Ok(())
    }
    async fn process_metric(&mut self, v: fsipc::legacy::StoreSignal) -> Result<(), meta::MError> {
        let args = v.args()?;
        let (key, value, when) = (args.key, args.value, args.when);
        debug!(target: "signal_loop", "legacy::StoreSignal from dbus, {}, {}, {}", key, value, when);
        #[allow(clippy::cast_precision_loss)]
        let when = when as f64;
        self.process_one_metric(key, value, when).await?;
        Ok(())
    }

    async fn process_metric_batch(
        &mut self,
        v: fsipc::logger1::StoreSignal,
    ) -> Result<(), meta::MError> {
        let args = v.args()?;
        debug!(target: "signal_loop", "logger1::StoreSignal from dbus, {:?}", args.batch);
        for (key, value, when) in args.batch {
            if let Some(svalue) = zvariant_to_string(&value) {
                #[allow(clippy::cast_precision_loss)]
                let when = when as f64;
                self.process_one_metric(&key, &svalue, when).await?;
            } else {
                debug!(target: "signal_loop", "Ignored due to wrong value type: {:?}", value);
            }
        }
        Ok(())
    }

    fn meta_len(&self) -> usize {
        self.meta.cache_len()
    }
}

pub(crate) async fn run_signal_actor(mut actor: SignalActor) -> Result<(), meta::MError> {
    const LOG_INTERVAL: Duration = Duration::from_secs(120);
    let ipc = fsipcProxy::builder(&actor.connection).build().await?;
    let logger1 = Logger1Proxy::builder(&actor.connection).build().await?;
    let mut metrics = ipc.receive_store_signal().await?;
    let mut metrics_batch = logger1.receive_store_signal().await?;
    let mut interval = tokio::time::interval(LOG_INTERVAL);
    ipc.store("modio.mqttbridge.store", "online").await?;
    ipc.store(METADATA_KEY, "0").await?;
    update_metadata_names(&actor.connection).await?;

    info!(target: "signal_loop", "Proxy is ready, awaiting signals, key filters: {:?}", &actor.matches);

    loop {
        tokio::select! {
            Some(metric) = metrics.next() => {
                actor.process_metric(metric).await?;
            },
            Some(batch) = metrics_batch.next() => {
                actor.process_metric_batch(batch).await?;
            },
            _ = interval.tick() => {
                info!(target: "signal_loop", "Processed metrics={}, metadata={}, metadata queue capacity={}/{}",
                      actor.metrics_count,
                      actor.meta_len(),
                      actor.meta.sender.capacity(),
                      actor.meta.sender.max_capacity()
                );
            },
            else => { break }
        };
    }
    panic!("run_signal_actor: No more signals for us?")
}

pub(crate) struct TransactionActor {
    receiver: mpsc::Receiver<Transaction>,
    connection: zbus::Connection,
}

impl TransactionActor {
    pub fn new(connection: zbus::Connection, receiver: mpsc::Receiver<Transaction>) -> Self {
        Self {
            receiver,
            connection,
        }
    }

    async fn handle_transaction(&mut self, data: Transaction) -> Result<(), zbus::Error> {
        info!(target: "transaction_loop", "Pushing Transaction to the logger: {:?}", &data);
        let ipc = fsipcProxy::builder(&self.connection).build().await?;
        ipc.transaction_add(&data.n, &data.expected(), &data.value(), &data.token)
            .await?;
        Ok(())
    }
}

pub(crate) async fn run_transaction_actor(mut actor: TransactionActor) -> Result<(), zbus::Error> {
    let ipc = fsipcProxy::builder(&actor.connection).build().await?;
    ipc.store("modio.mqttbridge.transaction", "online").await?;
    info!(target: "transaction_loop", "Proxy is ready, awaiting new transactions");
    while let Some(data) = actor.receiver.recv().await {
        actor.handle_transaction(data).await?;
    }
    panic!("run_transaction_actor: MQTT side of Transaction receive hung up.")
}
