# Deplyoment

modio-mqttbridge is configured and deployed via modio-contain from the clientconfig repo

See:
-  https://gitlab.com/ModioAB/clientconfig/

There you need to create an entry in the client.contain.ini for the device, like this:
   https://gitlab.com/ModioAB/clientconfig/-/blob/master/examples/client.contain.ini

With the entry being `modio-mqttbridge = main`


On it's own, that will fail, as it ALSO needs a config file for the device:
    example https://gitlab.com/ModioAB/clientconfig/-/blob/master/examples/modio-mqttbridge.ini

If the device has TLS cert/key requirements for some special case, they need to be placed on the device in:

    00ffffffff:/modio/private/modio-mqttbridge/ca.crt
    00ffffffff:/modio/private/modio-mqttbridge/client.crt
    00ffffffff:/modio/private/modio-mqttbridge/client.key

The files need to have permissions that allow the DynamicUser "modio-mqttbridge" to have access to it.

# Output

Output format is a limited version of SenML
<https://www.rfc-editor.org/rfc/rfc8428> , containing the following data:


    {
         "bn":  "urn:dev:mac:XXXXXXXXXX:"
         "n":  "yyy.zzz.yyy",
         "vs": "abc1234",
         "t": 1636722945.0,
         "u": "unit",
         "name": "See previous page in PDF"
    }


|key   | datatype | Description                |
|------|----------|-----------------------------|
| bn   | String   | Contains the urn for the mac of the device, and should end with a colon. (see <https://datatracker.ietf.org/doc/html/rfc8141> for details about urn) |
| n    | String   | will be the modio API style key format of the data point  |
| vs   | String   | Value, string format (Mostly string format is reported in the modio system due to legacy reasons) |
| t    | f64      | timestamp as unixtime                     |
| u    | String   | Unit, if available, senml supported unit  |
| v    | numeric  | value, numeric                            |
| vb   | bool     | value, boolean                            |
| vd   | binary   | value, data form, unused.                 |
| name | String   | Contains the Display string, usually from a data-sheet or similar. May be nonsense. |


# Topics

Each device has a configuration associated prefix that gets pre-pended for all datapoints.

prefix  = "demo-demo/some/demo"  
key = "21.12345"  
Then, metrics are added to the "/metric/" prefix and finally joined with the modio key split apart:

    demo-demo/some/demo/metric/21/12345

# Transactions

The broker will subscribe to a topic and listen to incoming messages with valid Change requests in them.

The topic will be built similarly like above:  
    prefix = "demo-demo/some/demo"  
        will result in the topic becoming:  
        demo-demo/some/demo/change/legacy  

Legacy is a signal that it is using the legacy data format which is subject to
change with minimal notification.
The data should be receieved in the form:

    {
        "n":  "key.for.item",
        "expected": "0",
         "token":"6d27d3dda40943a499059951211d5058",
         "v":321
    }

Where "n" is the key in the modio API style,
"expected":  can contain either (string|numeric|bool) data and should be understood to mean "the value to change from".

"token" is expected to be a UUID, preferrably in non-dashed hexadecimal form.
Reusing the same UUID will cause the transaction to fail.

Many loggers will prompt a re-read of the remote device before writing, and
a mismatch in precision, datatype or age of the transaction will cause it to
fail. Ie, reporting 1.0 where the remote device expects "1" _will_ be cause for
failure without further reason.

In this data-format, the system will allow one of (v | vs | vb)  according to
SenML rules, as the legacy system expects a strict string equality of data,
it's recommended to make sure that the change is a valid one. For example, if
storing a numeric datatype with precision issues to a meter that expects a
fix-point decimal scaled value, there will be a mismatch, and the backend
logger may reject it without further notice.

Dealing with numerical precision across systems is left as an exercise for the
reader.

Validating that the written data is inside a valid range for the remote unit is
expected to have happened before the data reaches this device. Knowing the
detail of whether the accepted range is 1,2,3 or something else, as well as
validating that it matches the device expectation should already have been done
before the request reaches this device.

Writing to a key that is considered read-only may have unintended consequences.
It is expected that the user knows exactly which keys are read only and read
write and treats such with care.

Writing to a key that changes hardware configuration may brick the device on
the other side. See above warning about read-only.

Writing to a key which is write-only may have other interesting consequences.
It is assumed that any user of this interface knows about such consequences in
detail.

Authentication, Authorization and Audit logging is not implemented on the
device. It is assumed that the user has done that already before data reaches
this device.

The modio MQTT bridge is designed to run only on the local device, with only
vetted code communicating with it.

Exposing it to traffic from other devices may have interesting side effects.

Results are currently not reported other than as a subsequent store result.


# Metadata

Metadata is queried locally and then published the first time the running process sees
a new key for a value.

It is published to the topic below `${prefix}/metadata/` in the same form as metric.

So the key `modbus.thirtynine.Multical_COOL_x03.4.2` becomes `${prefix}/metadata/modbus/thirtynine/Multical_COOL_x03/4/2`.

The metadata payload is in SenML-similar JSON structure, example:

    {
        "n":"modbus.TA3.climatix_IVPAHU.4.385",
        "name":"Fans Energy added today",
        "mode":"ro"
    }

The fields that are currently supported are:

|key          |  datatype            | Description|
|-------------|----------------------|------------|
|n            | String               | The key in the modio system, same as in payload.  |
|u            | String               | Unit  (Iana-registered SenML units only)          |
|name         | String               | Name of the key, from a data sheet.               |
|description  | String               | Description of the key, from a data sheet.        |
|value_map    | Map of u32 to String | Lookup for enums or other things that map an int to an String. |
|mode         | String "ro"/"rw"/"wo"| Can be read-only/write-only/read-write  state of key. |

All fields except "n" are considered Optional and may be absent.  In case no metadata is found, there will be no metadata payload delivered.


# Debugging

On device, to adjust commandline or environment var, use:
    `systemctl edit --runtime modio-mqttbridge.service` 
and add:

    [Service]
    ExecStart=
    ExecStart=/opt/bin/modio-mqttbridge --config /etc/modio-mqttbridge.conf -v -v -v

This example overrides the -v flags to be more verbose than our default. The
first, blank, ExecStart= is to disable the previous setting rather than to add to it.

Other edits that could be done are, fex:

    [Service]
    Environment=RUST_LOG=debug,rumqttc::state=info

the above example sets the default log to "debug" except for rumqttc::state
which is set to "info". More filters can be found in the
[rust env_logger](https://docs.rs/env_logger/latest/env_logger/index.html#enabling-logging) manual.


To revert your service changes again, simply edit your override, using  fex.
`systemctl edit --runtime modio-mqttbridge.service` and leave an empty `[Service]` section

    [Service]

This lets systemd use the defaults.
