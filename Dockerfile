FROM --platform=linux/arm registry.gitlab.com/modioab/scada-hello/template:armhf

ARG URL=unknown
ARG COMMIT=unknown
ARG BRANCH=unknown
ARG HOST=unknown
ARG DATE=unknown
ARG TARGET=armv7-unknown-linux-gnueabihf

LABEL "se.modio.ci.url"=$URL "se.modio.ci.branch"=$BRANCH "se.modio.ci.commit"=$COMMIT "se.modio.ci.host"=$HOST "se.modio.ci.date"=$DATE "se.modio.service"=modio-mqttbridge

COPY config/hosts /etc/hosts
COPY config/resolv.conf /etc/resolv.conf
COPY config/machine-id	/etc/machine-id
COPY config/modio-mqttbridge.conf /etc/modio-mqttbridge.conf
COPY config/modio-mqttbridge.service /etc/systemd/system/modio-mqttbridge.service
COPY config/modio-mqttbridge.path /etc/systemd/system/modio-mqttbridge.path

COPY target/${TARGET}/release/modio-mqttbridge /opt/bin/modio-mqttbridge
